package deloitte.academy.lesson01.logic;

public enum Tipos {
	UNO("Nuevo"), DOS("Frecuente"), TRES("Empleado");
	
	private Tipos() {
		// TODO Auto-generated constructor stub
	}
	
	public String tipo;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	private Tipos(String tipo) {
		this.tipo = tipo;
	}
	
}
