package deloitte.academy.lesson01.logic;

import deloitte.academy.lesson01.entity.Hotel;
import deloitte.academy.lesson01.run.CorrerHotel;

/**
 * Clase empleado: Permite registrar a los empleados.
 * @author jenrubio
 *
 */
public class Empleado extends Hotel{
	
	public Empleado() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Empleado(int importe, String nombre) {
		super(importe, nombre);
		// TODO Auto-generated constructor stub
	}

	/**
	 * M�todo perteneciente a la clase Hotel.
	 * Se le genera un descuento del 35% a 
	 * los empleados. 
	 */
	@Override
	public double checkIn() {
		double total = 0;
		total = this.getImporte() * 0.65;
		
		return total;
	}	
	

}
