package deloitte.academy.lesson01.logic;

import deloitte.academy.lesson01.entity.Hotel;
import deloitte.academy.lesson01.run.CorrerHotel;

/**
 * Clase que permite registrar un Huesped frecuente.
 * @author jenrubio
 *
 */
public class HuespedFrecuente extends Hotel{
	
	public HuespedFrecuente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HuespedFrecuente(int importe, String nombre) {
		super(importe, nombre);
		// TODO Auto-generated constructor stub
	}

	/**
	 * M�todo perteneciente a la clase Hotel.
	 * Se le genera un descuento del 25%
	 * a los clientes frecuentes.
	 */
	@Override
	public double checkIn() {
		double total = 0;

		total = this.getImporte() * 0.75;

		return total;
	}
	

}
