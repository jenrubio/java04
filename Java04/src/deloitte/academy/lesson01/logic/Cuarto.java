package deloitte.academy.lesson01.logic;

public class Cuarto {
	private int numero;
	private String disponible;
	
	public Cuarto() {
		// TODO Auto-generated constructor stub
	}

	public Cuarto(int numero, String disponible) {
		super();
		this.numero = numero;
		this.disponible = disponible;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getDisponible() {
		return disponible;
	}

	public void setDisponible(String disponible) {
		this.disponible = disponible;
	}
	

}
