package deloitte.academy.lesson01.logic;

import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson01.entity.Hotel;
import deloitte.academy.lesson01.run.CorrerHotel;

/**
 * Clase que permite asignar una clase a un cliente frecuente
 * o a un Empleado. Al mismo tiempo, manda a llamar al m�todo 
 * CheckIn de su respectivo objeto para calcular el precio total
 * ya con descuento. 
 * @author jenrubio
 *
 */
public class RegistrosHotel {
	private static final Logger LOGGER = Logger.getLogger(Logger.class.getName());
	private static int disp;
	public static int disponible = 0;
	private static double total;
	
	/**
	 * M�todo para verificar si existe el Empleado en la lista existente
	 * de Empleados. Si es as�, permite registrarlo en una de las habitaciones
	 * disponibles. Una vez registrado cambia el status de la habitaci�n a O
	 * lo cual significa (ocupado). D (disponible).
	 * @param empleado: Objeto de la clase Empleado
	 * Incluye el importe a pagar y su nombre. 
	 * @return total: Regresa el total a pagar ya con el descuento aplicado. 
	 * Si el empleado no existe regresa 0 (cero).
	 */
	public double agregarEmpleado(Empleado empleado) {
		total = 0;
		try {
			disponible = habitacionesDisp();
			if(disponible == 0) {
				LOGGER.info("No hay habitaciones disponibles");
			}
			else{
				for (Empleado empleado1 : CorrerHotel.registroHotelE) {
					if (empleado.getNombre() == empleado1.getNombre()) {
						total = empleado.checkIn();
						for(Cuarto cuartito : CorrerHotel.cuartos) {
							if(cuartito.getNumero() == disponible) {
								cuartito.setDisponible("O");
								CorrerHotel.registroTotal.add(empleado.getNombre() + " " + disponible);
							}
						}
						LOGGER.info("Habitaci�n apartada");
						break;
					} 
				}
			}
		}catch(Exception ex) {
				LOGGER.log(Level.SEVERE, "Ocurri� un error, vuelve a intentarlo", ex);
		}
		
		return total;
	}
	
	/**
	 * M�todo que permite checar si el huesped a registrar
	 * se encuentra en la lista de Huesped Frecuente. De ser as�,
	 * se le aplica el descuento correspondiente. Si no, regresa un
	 * total de 0 (cero). 
	 * Al mismo tiempo, agrega a este huesped a la lista de registro Total
	 * y cambia en la lista cuartos el estatus de la habitaci�n.
	 * @param huesped: Objeto tipo Huesped.
	 * @return total: Cantidad a pagar.
	 */
	public double descuentoHuesped(HuespedFrecuente huesped) {
		total = 0;
		try {
			disponible = habitacionesDisp();
			if(disponible != 0) {
				for (HuespedFrecuente hFrec : CorrerHotel.huespedFrec) {
					if (hFrec.getNombre() == huesped.getNombre()) {
						total = huesped.checkIn();
						for(Cuarto cuartito : CorrerHotel.cuartos) {
							if(cuartito.getNumero() == disponible) {
								cuartito.setDisponible("O");
								CorrerHotel.registroTotal.add(huesped.getNombre() + " " + disponible);
							}
						}
						LOGGER.info("Huesped frecuente: Habitaci�n apartada.");
						break;
					} 
					
				}
			}
			else {
				LOGGER.info("No hay habitaciones disponibles.");
			}
			}catch(Exception ex) {
				LOGGER.log(Level.SEVERE, "Ocurri� un error, vuelve a intentarlo", ex);
		}
		
		return total;
	}
	
	/**
	 * M�todo para checar las habitaciones disponibles
	 * @return disp: n�mero de habitaci�n disponible. 
	 */
	public int habitacionesDisp() {		
		for(Cuarto cuarto : CorrerHotel.cuartos) {
			if(cuarto.getDisponible() == "D") {
				disp = cuarto.getNumero();
				break;
			}
			else {
				disp = 0;
			}
		}
		
		return disp;
	}
	
	/**
	 * M�todo para poner alguna habitaci�n disponible.
	 */
	public void checkOut(int numH) {		
		for(Cuarto cuarto : CorrerHotel.cuartos) {
			if(cuarto.getNumero() == numH) {
				cuarto.setDisponible("D");
				break;
			}
		}
	}

}
