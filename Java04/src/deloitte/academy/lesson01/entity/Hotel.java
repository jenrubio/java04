package deloitte.academy.lesson01.entity;
/**
 * Clase abstracta que permite crear un Empleado
 * o un Huesped Frecuente. 
 * @author jenrubio
 *
 */
public abstract class Hotel {
	private int importe;
	private String nombre;

	//Constructores
	public Hotel() {
		// TODO Auto-generated constructor stub
	}

	public Hotel(int importe, String nombre) {
		super();
		this.importe = importe;
		this.nombre = nombre;
	}

	public int getImporte() {
		return importe;
	}

	public void setImporte(int importe) {
		this.importe = importe;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * M�todo abstracto que permitir� que se calcule
	 * el total a pagar.
	 * @return
	 */
	public abstract double checkIn();
}
