package deloitte.academy.lesson01.run;

import java.util.ArrayList;
import java.util.List;

import deloitte.academy.lesson01.logic.Cuarto;
import deloitte.academy.lesson01.logic.Empleado;
import deloitte.academy.lesson01.logic.HuespedFrecuente;
import deloitte.academy.lesson01.logic.RegistrosHotel;
import deloitte.academy.lesson01.logic.Tipos;

/**
 * Main para el registro de Huespedes, Empleados y Huespedes frecuentes del hotel.
 * Permite mandar llamar los m�todos de RegistrosHotel.
 * @author jenrubio
 *
 */
public class CorrerHotel {

	//Listas 
	public static final List<Empleado> registroHotelE = new ArrayList<Empleado>();
	public static final List<String> registroTotal = new ArrayList<String>();
	public static final List<HuespedFrecuente> huespedFrec = new ArrayList<HuespedFrecuente>();
	public static final List<Cuarto> cuartos = new ArrayList<Cuarto>();
	
	
	public static void main(String[] args) {
		String tipo = "";
		double total = 0;
		RegistrosHotel registro = new RegistrosHotel();
		
		
		//Creaci�n de cuartos
		Cuarto cuarto1 = new Cuarto(1, "D");
		cuartos.add(cuarto1);
		Cuarto cuarto2 = new Cuarto(2, "O");
		cuartos.add(cuarto2);
		Cuarto cuarto3 = new Cuarto(3, "D");
		cuartos.add(cuarto3);
		Cuarto cuarto4 = new Cuarto(4, "D");
		cuartos.add(cuarto4);
		Cuarto cuarto5 = new Cuarto(5, "O");
		cuartos.add(cuarto5);
		Cuarto cuarto6 = new Cuarto(6, "D");
		cuartos.add(cuarto6);
		Cuarto cuarto7 = new Cuarto(7, "D");
		cuartos.add(cuarto7);
		Cuarto cuarto8 = new Cuarto(8, "D");
		cuartos.add(cuarto8);
		
		//Registro con habitaciones ocupadas
		registroTotal.add("Ana Barrera " + cuarto2.getNumero() +"\n");
		registroTotal.add("Fernanda Garcia " + cuarto5.getNumero() + "\n");
		
		//Lista de empleados
		Empleado emp1 = new Empleado(200, "Ana Barrera");
		registroHotelE.add(emp1);
		Empleado emp2 = new Empleado(200, "Barbara Calles");
		registroHotelE.add(emp2);
		Empleado emp3 = new Empleado(200, "Claudia Diaz");
		registroHotelE.add(emp3);
		Empleado emp4 = new Empleado(200, "Diana Espinoza");
		registroHotelE.add(emp4);
		
		//Lista clientes frecuentes
		HuespedFrecuente hf1 = new HuespedFrecuente(300, "Esmeralda Fernandez");
		huespedFrec.add(hf1);
		HuespedFrecuente hf2 = new HuespedFrecuente(300, "Fernanda Garcia");
		huespedFrec.add(hf2);
		HuespedFrecuente hf3 = new HuespedFrecuente(300, "Gabriela Hernandez");
		huespedFrec.add(hf3);
		HuespedFrecuente hf4 = new HuespedFrecuente(300, "Hilda Iruzca");
		huespedFrec.add(hf4);
		HuespedFrecuente hf5 = new HuespedFrecuente(300, "Ingrid Jimenez");
		huespedFrec.add(hf5);
		
		
		/*
		 * UNO : Cliente nuevo
		 * DOS : Cliente frecuente
		 * TRES: Empleado
		 * Escribir el tipo de usuario
		 */
		
		//tipo = Tipos.UNO.getTipo();
		tipo = Tipos.DOS.getTipo();
		//tipo = Tipos.TRES.getTipo();
		
		if(tipo == "Nuevo") {
			int habDisp = registro.habitacionesDisp();
			if(habDisp != 0) {
				for(Cuarto cuartito : cuartos) {
					if(cuartito.getNumero() == habDisp) {
						cuartito.setDisponible("O");
						
						//cantidadPagar define el costo de esa habitaci�n. 
						int cantidadPagar = 350;
						registroTotal.add("Juan P�rez".concat(" " + cuartito.getNumero()).concat("\n"));
						System.out.println("Total a pagar: " + cantidadPagar);
					}
				}
			}
		}
		else if(tipo == "Frecuente") {
			//Existe: Ingrid Jimenez
			//No existe: Jennifer Rubio
			HuespedFrecuente huesped1 = new HuespedFrecuente(300, "Ingrid Jimenez");
			total = registro.descuentoHuesped(huesped1);
			if(total != 0) {
				System.out.println("Cliente: "+ huesped1.getNombre() +" Total a pagar: " + total + " Habitaci�n: " + registro.disponible);
			}else {
				System.out.println("Cliente frecuente NO existente");
			}
			
		}
		else {
			//No existe: Diana Espinoza
			//Existe: Gabriela Hernandez
			Empleado empN = new Empleado(200, "Diana Espinoza");	
			total = registro.agregarEmpleado(empN);
			if(total != 0) {
				System.out.println("Empleado: " + empN.getNombre() +"Total a pagar: " + total + " Habitaci�n: " + registro.disponible);
			}
			else {
				System.out.println("Empleado no existente");
			}
			
		}
		
		//Imprimir los resultados
		System.out.println("\n\nRegistro: ");
		System.out.println(registroTotal);
		
		//Lista cuartos sin checkout
		System.out.println("\n\nLista cuartos: ");
		for(Cuarto c : cuartos) {
			System.out.println("N�mero de cuarto: " + c.getNumero()+ " Estatus: "+ c.getDisponible());
		}
		
		System.out.println("\n\nLista Empleados: ");
		for(Empleado em : registroHotelE) {
			System.out.println(em.getNombre());
		}
		
		System.out.println("\n\nLista Clientes Frecuentes: ");
		for(HuespedFrecuente hf: huespedFrec) {
			System.out.println(hf.getNombre());
		}
		

		
		//Lista de cuartos despu�s de Checkout
		System.out.println("\n\nLista Cuartos: ");
		registro.checkOut(1);
		for(Cuarto c : cuartos) {
			System.out.println("N�mero de cuarto: " + c.getNumero()+ " Estatus: "+ c.getDisponible());
		}
	}

}
